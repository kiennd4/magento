define(
    [
        'jquery',
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote'
    ],
    function ($,Component,quote) {
        "use strict";
        return Component.extend({
            defaults: {
                template: 'Mageplaza_GiftCard/checkout/summary/customdiscount'
            },
            quoteIsVirtual: quote.isVirtual(),
            totals: quote.getTotals(),
            isDisplayedCustomdiscount : function(){
                return true;
            },
            getCustomDiscount : function(){
                var segments = this.totals()['total_segments'];
                for(var i = 0; i < segments.length; i++) {
                    var total = segments[i];
                    if (total['title'] === ''){
                        var price =  total['value'];
                        return this.getFormattedPrice(price);
                    }
                }
            }
        });
    }
);
