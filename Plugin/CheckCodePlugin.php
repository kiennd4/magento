<?php

namespace Mageplaza\GiftCard\Plugin;
class CheckCodePlugin
{
    protected $_checkoutSession;
    public function __construct(
        \Magento\Checkout\Model\Session             $checkoutSession
    )
    {
        $this->_checkoutSession = $checkoutSession;
    }
    public function afterGetCouponCode(\Magento\Checkout\Block\Cart\Coupon $subject, $result)
    {
        $model = $this->_checkoutSession->getData('coupon_code');
        $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/custom.log');
        $logger = new \Zend_Log();
        $logger->addWriter($writer);
        $logger->info(json_encode($model));

        if ($model) {
            return $model;
        }else{
            return $result;
    }
    }
}
