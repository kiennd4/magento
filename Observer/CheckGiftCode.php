<?php

namespace Mageplaza\GiftCard\Observer;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\ActionFlag;

class CheckGiftCode implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;

    protected $_messageManager;

    protected $_actionFlag;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    protected $_giftcardFactory;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     */

    protected $_checkoutSession;
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ActionFlag $actionFlag,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Magento\Checkout\Model\Session             $checkoutSession,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
    )
    {
        $this->_messageManager = $messageManager;
        $this->_actionFlag = $actionFlag;
        $this->redirect = $redirect;
        $this->_checkoutSession = $checkoutSession;
        $this->_giftcardFactory = $giftcardFactory;
    }

    /**
     * customer register event handler
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        // get enetered gift code
        $controller = $observer->getControllerAction();
        /** @var Action $controller */
        $controller->getRequest()->getControllerName();
        $couponCode = $controller->getRequest()->getParam('coupon_code');
        $giftcard = $this->_giftcardFactory->create();
        $db_coupen_code = $giftcard->load($couponCode, 'code');

        if(!empty($db_coupen_code->getData('code'))) {
                $this->_checkoutSession->setData('coupon_code', $couponCode);
                $this->_checkoutSession->setData('balance', $db_coupen_code->getData('balance'));
                $this->_messageManager->addSuccess(__("Gift code applied successfully"));
                $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
                $this->redirect->redirect($controller->getResponse(), 'checkout/cart');
                return;
            }

        if ($controller->getRequest()->getParam('remove') == 1) {
            $this->_checkoutSession->setData('coupon_code', '');
            $this->_checkoutSession->setData('balance', '');
        }


    }
}
