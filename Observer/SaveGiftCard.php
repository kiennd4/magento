<?php
/**
 * Save Custom Fee in sales_order before place an order.
 */

namespace Mageplaza\GiftCard\Observer;

use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class SaveGiftCard implements ObserverInterface
{
    protected $_giftcardFactory;

    protected $_historyFactory;

    protected $_scopeConfig;

    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $_customerSession;

    /**
     * @param Context $context
     * @param \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
     * @param \Mageplaza\GiftCard\Model\HistoryFactory $historyFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        Context $context,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory,
        \Mageplaza\GiftCard\Model\HistoryFactory $historyFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Customer\Model\Session $customerSession,
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_giftcardFactory = $giftcardFactory;
        $this->_historyFactory = $historyFactory;
        $this->_customerSession = $customerSession;

    }

    /**
     * @param Observer $observer
     * @return $this|void
     * @throws \Zend_Log_Exception
     */

    public function getGiftCode($length) {
        return $this->newCode = substr(str_shuffle('ABCDEFGHIJKLMLOPQRSTUVXYZ0123456789'),0 , $length);
    }
    public function execute(Observer $observer)
    {
        // Get Order Object
        /* @var $order \Magento\Quote\Model\Quote */
        $order = $observer->getEvent()->getData('quote');
        $items = $order->getItems();
        try{
            foreach ($items as $item) {
                for ($i = 0; $i < $order->getItemsQty(); $i++) {
                    $giftcard = $this->_giftcardFactory->create();
                    $history = $this->_historyFactory->create();
                    $data = [
                        'code' => $this->getGiftCode($this->_scopeConfig->getValue('giftcard/code/text',
                            \Magento\Store\Model\ScopeInterface::SCOPE_STORE)),
                        'balance' => $item->getPrice(),
                        'create_from' => "created from " . $order->getReservedOrderId()
                    ];
                    $giftcard->addData($data)->save();

                    $dataHistory = [
                        'giftcard_id' => $giftcard->getId(),
                        'customer_id' => $this->_customerSession->getCustomer()->getId(),
                        'amount' => $giftcard->getData('balance'),
                        'action' => 'Used from order by #' . $order->getReservedOrderId(),
                    ];
                    $history->addData($dataHistory)->save();
                }
            }
        }catch (\Exception $e){
            $writer = new \Zend_Log_Writer_Stream(BP . '/var/log/custom.log');
            $logger = new \Zend_Log();
            $logger->addWriter($writer);
            $logger->info($e->getMessage());
        }

        return $this;
    }
}
