<?php

namespace Mageplaza\GiftCard\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $_giftcardFactory;

    public function __construct(\Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory)
    {
        $this->_giftcardFactory = $giftcardFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
            'code'         => "14092001",
            'balance' => 1490000,
            'amount_used'      => 149000,
            'create_from'         => 'admin',
        ];
        $giftcard = $this->_giftcardFactory->create();
        $giftcard->addData($data)->save();
    }
}
