<?php
namespace Mageplaza\GiftCard\Controller\Adminhtml\Code;
use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultFactory;
class Newcode extends Action
{
    protected $request;
    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
    ) {
        parent::__construct($context);
        $this->request = $request;
        $this->coreRegistry = $coreRegistry;
        $this->giftcardFactory = $giftcardFactory;
    }
    public function getIddata()
    {
        // use
        $this->request->getParams();
        return $this->request->getParam('id');
    }
    public function execute()
    {
        $keyId = $this->getIddata();
        $giftcardId = $this->getRequest()->getParam('giftcard_id');
        if ($keyId) {
            $giftcardModel = $this->giftcardFactory->create()->load($giftcardId);
            $this->coreRegistry->register('giftcard_action', $giftcardModel);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->prepend(__("Edit Gift Card"));
        }else {
            $giftcardModel = $this->giftcardFactory->create()->load($giftcardId);
            $this->coreRegistry->register('giftcard_action', $giftcardModel);
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $resultPage->getConfig()->getTitle()->prepend(__("New Gift Card"));
        }
        return $resultPage;
    }

}
