<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
/**
 * Created By : Rohan Hapani
 */
namespace Mageplaza\GiftCard\Controller\Adminhtml\Code;
use Magento\Backend\App\Action;
use Magento\Backend\Model\Auth\Session;
use PharIo\Version\Exception;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_adminSession;
    /**
     * @var \Mageplaza\GiftCard\Model\GiftCardFactory
     */
    protected $giftcardFactory;
    /**
     * @param Action\Context                      $context
     * @param \Magento\Backend\Model\Auth\Session $adminSession
     * @param \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
     */
    protected $request;

    public function __construct(
        Action\Context $context,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Backend\Model\Auth\Session $adminSession,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
    ) {
        parent::__construct($context);
        $this->_adminSession = $adminSession;
        $this->giftcardFactory = $giftcardFactory;
        $this->request = $request;
    }
    /**
     * @return \Magento\Backend\Model\View\Result\Redirect
     */

    protected $newCode;
    public function getGiftCode($length) {
                return $this->newCode = substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVXYZ0123456789'),0 , $length);
    }
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $model = $this->giftcardFactory->create();

            $id = $this->getRequest()->getParam('giftcard_id');
            if ($id) {
                $model->load($id);
            } else {
                $data['code'] = $this->getGiftCode($data['code_length']);
            }
            $model->setData($data);
            try {
                $model->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->_adminSession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('giftcard/*/newcode', ['id' => $model->getId(), '_current' => true, 'data' => $data]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }
            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')],);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
