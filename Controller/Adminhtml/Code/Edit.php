<?php
namespace Mageplaza\GiftCard\Controller\Adminhtml\Code;

class Edit extends \Magento\Backend\App\Action
{

    protected $_resultPageFactory;

    protected $_resultForwardFactory;

    public function __construct(
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Mageplaza\GiftCard\Model\GiftCardFactory $codeFactory,
        \Magento\Backend\App\Action\Context $context
    )
    {
        $this->_resultForwardFactory = $resultForwardFactory;
        $this->_resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        $this->codeFactory = $codeFactory;
        parent::__construct($context);
    }


    public function execute()
    {
        $resultPage = $this->_resultForwardFactory->create();
        $resultPage->forward('newcode');
        return $resultPage;
    }
}
