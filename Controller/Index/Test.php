<?php
namespace Mageplaza\GiftCard\Controller\Index;

use Magento\Framework\App\Action\Context;

class Test extends \Magento\Framework\App\Action\Action
{
    protected $_giftcardFactory;

    public function __construct(
        Context $context,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
    )
    {
        $this->_giftcardFactory = $giftcardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $code = "ht1409";
        $giftcard = $this->_giftcardFactory->create();
        dd($giftcard->load($code, 'code')->getData());
        $data = [
            'code' => "cl122k3",
            'balance' => 1200000,
            'amount_used' => 120000,
            'create_from' => "darknight"
        ];

        try{
//            $giftcard->addData($data)->save();
            $code = "ht1409";
//            load('code', $code);
            $a = $giftcard->loadByCode($code);
            print_r($a);
            if($giftcard->getData('giftcard_id')){
//                $giftcard->delete();
                $giftcard->setData('code','12')->save();
                echo "Success!";
            }else {
                echo "Gift Card ID does not exist!";
            }
        }catch (\Exception $e){
            echo "Error!";
        }

    }
}
