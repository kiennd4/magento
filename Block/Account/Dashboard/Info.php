<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\GiftCard\Block\Account\Dashboard;

use Magento\Framework\NumberFormatter;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ScopeInterface;


class Info extends Template
{

    protected $_customer;
    protected $_customerFactory;

    protected $_customerSession;
    /**
     * Constructor
     *
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Customer $customers,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_customerFactory = $customerFactory;
        $this->_customer = $customers;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context, $data);
    }

    public function DisplayReedem(){
        $display = $this->scopeConfig->getValue('giftcard/general/enable_r',
            ScopeInterface::SCOPE_STORE);
        return $display;
    }

    public function getBalance() {
        $data = $this->_customerFactory->create();
        $data->load($this->_customerSession->getCustomer()->getId());
        $data->getData('giftcard_balance');
        $number = $data->getData('giftcard_balance');
        $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($number, 'USD');

    }

}
