<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mageplaza\GiftCard\Block\Account\Dashboard;
use Magento\Framework\NumberFormatter;

/**
 * Class to manage customer dashboard addresses section
 *
 * @api
 * @since 100.0.2
 */
class History extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    protected $_customerSession;

   protected $_historyFactory;

   protected $_giftcardFactory;

    /**
     * @param \Mageplaza\GiftCard\Model\HistoryFactory $historyFactory
     * @param \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param array $data
     */
    public function __construct(
        \Mageplaza\GiftCard\Model\HistoryFactory $historyFactory,
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->_giftcardFactory = $giftcardFactory;
        $this->_historyFactory = $historyFactory;
        $this->_customerSession = $customerSession;
        parent::__construct($context, $data);
    }

    public function getGiftCard($id) {
        $giftcard = $this->_giftcardFactory->create();
        return $giftcard->load($id);
    }
    public function formatPrice($number) {
        $formatter = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($number, 'USD');
    }
    public function getHistory() {
        $history = $this->_historyFactory->create()->getCollection()->addFieldToFilter('customer_id', $this->_customerSession->getCustomer()->getId());
        $data = $history->getData();
        foreach($data as $value) {
            $value['giftcard_id'] = $this->getGiftCard($value['giftcard_id'])->getData('code');
        }
        return $data;
    }

}
