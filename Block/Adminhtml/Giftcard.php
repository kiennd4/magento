<?php

namespace Mageplaza\GiftCard\Block\Adminhtml;

class Giftcard extends \Magento\Backend\Block\Widget\Grid\Container
{
    protected function _construct()
    {
        $this->_controller = 'adminhtml_code';
        $this->_blockGroup = 'Mageplaza_GiftCard';
        $this->_headerText = __('New Gift Card');
        $this->_addButtonLabel = __('Save Gift Card');
        parent::_construct();
    }
}
