<?php

namespace Mageplaza\GiftCard\Block\Adminhtml\Action;

use Magento\Backend\Block\Widget\Form\Container;

class Newcode extends Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;
    protected $_paramId;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $coreregistry
     * @param array $data
     */
    public function __construct(
        \Mageplaza\GiftCard\Controller\Adminhtml\Code\Newcode $paramId,
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry           $coreregistry,
        array                                 $data = []
    )
    {
        $this->_paramId = $paramId;
        $this->_coreRegistry = $coreregistry;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'giftcard_id';
        $this->_blockGroup = 'Mageplaza_GiftCard';
        $this->_controller = 'adminhtml_action';

        parent::_construct();


        $this->buttonList->update('save', 'label', __('Save Gift Card'));
        if(!$this->_paramId->getIddata()) {
            $this->buttonList->add(
                'save_edit',
                [
                    'label' => __('Save and Continue Edit'),
                    'class' => 'save',
                    'data_attribute' => [
                        'mage-init' => [
                            'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                        ],
                    ],
                    'sort_order' => 80,
                ],

            );
        }


    }
    /**
     * Get header with Department name
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        if ($this->_coreRegistry->registry('giftcard_code')->getId()) {
            return __("Edit Gift Card '%1'", $this->escapeHtml($this->_coreRegistry->registry('giftcard_code')->getName()));
        } else {
            return __('New Gift Card');
        }
    }

    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */

    /**
     * Getter of url for "Save and Continue" button
     * tab_id will be replaced by desired by JS later
     *
     * @return string
     */
    protected function _getSaveAndContinueUrl()
    {
        return $this->getUrl('giftcard/code/save', ['_current' => true, 'back' => 'newcode', 'active_tab' => '{{tab_id}}']);
    }

    protected function _prepareLayout()
    {
        $this->_formScripts[] = "
        function toggleEditor() {
            if (tinyMCE.getInstanceById('page_content') == null) {
                tinyMCE.execCommand('mceAddControl', false, 'content');
            } else {
                tinyMCE.execCommand('mceRemoveControl', false, 'content');
            }
        };
        ";
        return parent::_prepareLayout();
    }
}
