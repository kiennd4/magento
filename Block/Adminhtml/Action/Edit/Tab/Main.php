<?php

namespace Mageplaza\GiftCard\Block\Adminhtml\Action\Edit\Tab;
use Magento\Backend\Block\Widget\Tab\TabInterface;

/**
 * Class Main
 * @package Mageplaza\GiftCard\Block\Adminhtml\Action\Edit\Tab
 */
class Main extends \Magento\Backend\Block\Widget\Form\Generic implements TabInterface
{
    protected $_paramId;
    protected $_giftcardFactory;
    public function __construct(
        \Mageplaza\GiftCard\Model\GiftCardFactory $giftcardFactory,
        \Mageplaza\GiftCard\Controller\Adminhtml\Code\Newcode $paramId,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry             $registry,
        \Magento\Framework\Data\FormFactory     $formFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        array                                   $data = []
    )
    {
        $this->_paramId = $paramId;
        $this->scopeConfig = $scopeConfig;
        $this->_giftcardFactory = $giftcardFactory;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    protected function _prepareForm()
    {
        /** @var \Mageplaza\GiftCard\Model\ResourceModel $model  */
        $model = $this->_coreRegistry->registry('giftcard_action');

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('action_');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Gift Card Information'), 'class' => 'fieldset-wide']
        );

        if ($this->_paramId->getIddata()) {
            $fieldset->addField(
                'code',
                'text',
                ['name' => 'code',
                    'label' => __('Code'),
                    'title' => __('Code'),
                    'disabled' => true,
                ]

            );
            $fieldset->addField(
                'balance',
                'text',
                ['name' => 'balance',
                    'label' => __('Balance'),
                    'title' => __('Balance')
                ]

            );
            $fieldset->addField(
                'giftcard_id',
                'hidden',
                ['name' => 'giftcard_id',
                ]

            );
            $fieldset->addField(
                'create_from',
                'text',
                ['name' => 'create_from',
                    'label' => __('Create From'),
                    'title' => __('Create From'),
                    'disabled' => true,
                ]

            );
            $giftcard = $this->_giftcardFactory->create();
            $giftcard->load($this->_paramId->getIddata());
            $model->setData('code',$giftcard->getData('code'));
            $model->setData('giftcard_id',$this->_paramId->getIddata());
            $model->setData('balance',$giftcard->getData('balance'));
            $model->setData('create_from',$giftcard->getData('create_from'));
        }else {
            $fieldset->addField('create_from', 'hidden', ['name' => 'create_from']);

            $fieldset->addField('amount_used', 'hidden', ['name' => 'amount_used']);


            $fieldset->addField(
                'code_length',
                'text',
                ['name' => 'code_length',
                    'label' => __('Code Length'),
                    'title' => __('Code Length'),
                    'class' => 'validate-number'
                ],

            );

            $fieldset->addField(
                'balance',
                'text',
                ['name' => 'balance',
                    'label' => __('Balance'),
                    'title' => __('Balance'),
                    'class' => 'validate-number',
                    'required' => true]
            );
            $model->setData('code_length',$this->scopeConfig->getValue('giftcard/code/text',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
            $model->setData('balance','200');
            $model->setData('create_from','admin');
        }








        $form->setValues($model->getData());
//        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Return Tab label
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabLabel()
    {
        return __('Gift Card Information');
    }
    /**
     * Return Tab title
     *
     * @return \Magento\Framework\Phrase
     */
    public function getTabTitle()
    {
        return __('Gift Card Information');
    }
    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        return true;
    }
    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return false;
    }
    /**
     * Check permission for passed action
     *
     * @param string $resourceId
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}

