<?php

namespace Mageplaza\GiftCard\Model\Total\Quote;
/**
 * Class Custom
 * @package Mageplaza\GiftCard\Model\Total\Quote
 */
class Custom extends \Magento\Quote\Model\Quote\Address\Total\AbstractTotal
{
    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $_priceCurrency;

    protected $_checkoutSession;


    /**
     * Custom constructor.
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Checkout\Model\Session $checkoutSession
    )
    {
        $this->_priceCurrency = $priceCurrency;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment
     * @param \Magento\Quote\Model\Quote\Address\Total $total
     * @return $this|bool
     */
    public function collect(
        \Magento\Quote\Model\Quote $quote,
        \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment,
        \Magento\Quote\Model\Quote\Address\Total $total
    )
    {
        $balance = $this->_checkoutSession->getData('balance');
        parent::collect($quote, $shippingAssignment, $total);
        if ($balance) {
            $customDiscount = -$balance;
        } else {
            $customDiscount = 0;
        }

        $total->addTotalAmount('customdiscount', $customDiscount);
//        $total->addBaseTotalAmount('customdiscount', $customDiscount);
        $total->setCustomDiscount( $customDiscount);
        $quote->setCustomDiscount($customDiscount);
        return $this;
    }

    public function fetch(\Magento\Quote\Model\Quote $quote, \Magento\Quote\Model\Quote\Address\Total $total)
    {
        return [
            'code' => $this->_checkoutSession->getData('coupon_code'),
            'title' => 'gift_code',
            'value' => $this->_checkoutSession->getData('balance')
        ];
    }

}
